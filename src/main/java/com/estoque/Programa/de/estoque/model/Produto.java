package com.estoque.Programa.de.estoque.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "produto")


public class Produto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Categoria categoria;
    private String nome;
    private Double preco;
    private Date validade;
    private Integer qtd;
    @ManyToOne
    @JsonBackReference
    private Estoque estoque;

    /*
        {
            "categoria":"REFRIGERANTE",
            "nome":"Coca-Cola",
            "preco":"12.00",
            "validade":"2023-01-01",
            "qtd":"30"
        }

     */
}
