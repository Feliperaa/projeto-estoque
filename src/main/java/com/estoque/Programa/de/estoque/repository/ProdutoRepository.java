package com.estoque.Programa.de.estoque.repository;

import com.estoque.Programa.de.estoque.model.Produto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

}
