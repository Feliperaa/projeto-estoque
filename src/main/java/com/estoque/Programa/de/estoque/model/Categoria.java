package com.estoque.Programa.de.estoque.model;

public enum Categoria {
    VINHO, CERVEJA, REFRIGERANTE, AGUAS, ESPUMANTE, DESTILADO, ENERGETICO, COMIDA, CIGARRO, UTILITARIOS
}
