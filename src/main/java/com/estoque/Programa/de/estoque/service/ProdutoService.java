package com.estoque.Programa.de.estoque.service;

import com.estoque.Programa.de.estoque.model.Produto;
import com.estoque.Programa.de.estoque.repository.ProdutoRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {
    @Autowired
    private ProdutoRepository produtoRepository;

    public List<Produto> listarProdutos() {
        return produtoRepository.findAll();
    }

    public Produto addProduto(Produto produto) {
        return produtoRepository.save(produto);
    }

    public void deletarProduto(Long id) {
        produtoRepository.deleteById(id);
    }

    public Produto editarProduto(Long id, Produto produto) {
        Produto auxProduto = validarProduto(id);
        BeanUtils.copyProperties(produto, auxProduto,"id");
        return produtoRepository.save(auxProduto);
    }

    public Produto validarProduto (Long id){
        Optional<Produto> produto = produtoRepository.findById(id);
        if(produto.isEmpty()){
            throw new EmptyResultDataAccessException(1);
        }
        return produto.get();
    }

    public Produto mostrarUmProduto(Long id) {
        return validarProduto(id);
    }
}
