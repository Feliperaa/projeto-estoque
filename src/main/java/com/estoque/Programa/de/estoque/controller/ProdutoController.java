package com.estoque.Programa.de.estoque.controller;

import com.estoque.Programa.de.estoque.model.Produto;
import com.estoque.Programa.de.estoque.service.ProdutoService;
import org.apache.coyote.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/produto")

public class ProdutoController {
    @Autowired
    private ProdutoService produtoService;

    @GetMapping
    public List<Produto> listarProdutos(){
       return produtoService.listarProdutos();
    }

    @GetMapping("/{id}")
    public Produto mostrarUmProduto(@PathVariable Long id){
        return produtoService.mostrarUmProduto(id);
    }

    @PostMapping
    public ResponseEntity<Produto> addProduto (@RequestBody Produto produto){
        Produto auxProd = produtoService.addProduto(produto);
        return ResponseEntity.status(HttpStatus.CREATED).body(auxProd);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Produto> editarProduto (@PathVariable Long id, @RequestBody Produto produto){
       return ResponseEntity.status(HttpStatus.ACCEPTED).body(produtoService.editarProduto(id,produto));
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarProduto (@PathVariable Long id){
        produtoService.deletarProduto(id);
    }

}
